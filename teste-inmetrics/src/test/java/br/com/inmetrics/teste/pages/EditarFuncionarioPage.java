package br.com.inmetrics.teste.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditarFuncionarioPage extends BasePage {

	@FindBy(xpath = "//button[@class=\"btn btn-warning\"]")
	protected WebElement btEditarFuncionario;
	
	@FindBy(xpath = "//*[@id=\"tabela_filter\"]//input[@type=\"search\"]")
	protected WebElement stPesquisar;
	
	@FindBy(id = "inputNome")
	protected WebElement stNome;
	
	@FindBy(id = "inputCargo")
	protected WebElement stCargo;

	@FindBy(name = "salario")
	protected WebElement stSalario;
	
	@FindBy(xpath = "//input[@class=\"cadastrar-form-btn\"]")
	protected WebElement btEnviar;
	
	@FindBy(xpath = "//div[@class=\"alert alert-success alert-dismissible fade show\"]")
	protected WebElement lbSuccessMessage;	
	
	public void clicarBtEditarFuncionario() {
		clickElement(btEditarFuncionario);
	}
	
	public void pesquisaFuncionario(String cpf) {
		clickElement(stPesquisar);
		setText(stPesquisar, cpf);
	}
	
	public void alterarNome(String Newnome) {
		clickElement(stNome);
		setText(stNome, Keys.CONTROL + "a");
		setText(stNome, Newnome);
	}
	
	public void alterarCargo(String Newcargo) {
		clickElement(stCargo);
		setText(stCargo, Keys.CONTROL + "a");
		setText(stCargo, Newcargo);
	}
	
	public void alterarSalario(String Newsalario) {
		clickElement(stSalario);
		setText(stSalario, Keys.CONTROL + "a");
		setText(stSalario, Newsalario);
	}
	
	public void clicarEnviar() {
		fixedWait(10);
		clickElement(btEnviar);
	}
	
	public String getSuccessMessage() {
		waitOn(lbSuccessMessage);
		return getText(lbSuccessMessage);
	}
	
}
