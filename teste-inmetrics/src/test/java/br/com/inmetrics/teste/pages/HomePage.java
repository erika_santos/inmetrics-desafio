package br.com.inmetrics.teste.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

	@FindBy(xpath = "//span[@class=\"login100-form-title\"]")
	protected WebElement lbCadastrese;
	
	@FindBy(xpath = "//div[@class=\"w-full text-center p-t-30\"]//a[contains(.,\"Cadastre-se\")]")
	protected WebElement btCadastrese;

	@FindBy(name = "username")
	protected WebElement stUsuario;

	@FindBy(name = "pass")
	protected WebElement stSenha;

	@FindBy(name = "confirmpass")
	protected WebElement stConfirmeSenha;

	@FindBy(xpath = "//span[@class=\"login100-form-title p-b-1\"]")
	protected WebElement lbLogin;

	@FindBy(xpath = "//button[contains(.,\"Entre\")]")
	protected WebElement btEntre;	
	
	@FindBy(xpath = "//div[@class=\"container-login100-form-btn m-t-17\"]//button[contains(., \"Cadastrar\")]")
	protected WebElement btCadastrar;	
	
	@FindBy(xpath = "//*[@id=\"tabela_filter\"]//input[@type=\"search\"]")
	protected WebElement stPesquisar;
	
	public boolean checkLbCadastrese() {
		return checkElementVisible(lbCadastrese);
	}
	
	public boolean checkLbLogin() {
		return checkElementVisible(lbLogin);
	}
	
	public boolean checkFuncionariosPage() {
		return checkElementVisible(stPesquisar);
	}

	public void clicarBtCadastrese() {
		clickElement(btCadastrese);
	}
	
	public void clicarBtEntre() {
		clickElement(btEntre);
	}
	
	public void clicarBtCdastrar() {
		clickElement(btCadastrar);
	}

	public void preencherUsuario(String user) {
		clickElement(stUsuario);
		setText(stUsuario, user);
	}
	
	public void preencherSenha(String password) {
		clickElement(stSenha);
		setText(stSenha, password);
	}
	
	public void confirmarSenha(String password) {
		clickElement(stConfirmeSenha);
		setText(stConfirmeSenha, password);
	}

	
}
