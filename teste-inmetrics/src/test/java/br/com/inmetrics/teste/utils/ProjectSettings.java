package br.com.inmetrics.teste.utils;

public enum ProjectSettings {
	WEBDRIVER("webdriver.chrome.driver"), 
	CHROME_DRIVER("src/test/resources/driver/chromedriver.exe"), 
	URL("http://www.inmrobo.tk/accounts/login/");

	private String configuration;

	ProjectSettings(String config) {
		this.configuration = config;
	}

	public String getConfig() {
		return configuration;
	}
}
