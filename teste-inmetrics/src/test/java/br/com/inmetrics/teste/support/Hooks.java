package br.com.inmetrics.teste.support;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import br.com.inmetrics.teste.utils.ProjectSettings;
import io.cucumber.java.After;
import io.cucumber.java.Before;


public class Hooks {

	private static WebDriver driver;

	@Before
	public void beforCenario() {
		System.setProperty(ProjectSettings.WEBDRIVER.getConfig(), ProjectSettings.CHROME_DRIVER.getConfig());
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(ProjectSettings.URL.getConfig());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	public static WebDriver getDriver() {
		return driver;
	}

	@After
	public void fecharAplicacao() {
		// driver.close();
		driver.quit();
	}

}