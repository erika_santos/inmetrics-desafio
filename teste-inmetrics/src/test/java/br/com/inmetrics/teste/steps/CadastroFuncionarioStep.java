package br.com.inmetrics.teste.steps;

import static org.junit.Assert.*;

import br.com.inmetrics.teste.pages.CadastroFuncionarioPage;
import br.com.inmetrics.teste.pages.DeletarFuncionarioPage;
import br.com.inmetrics.teste.pages.EditarFuncionarioPage;
import br.com.inmetrics.teste.pages.HomePage;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class CadastroFuncionarioStep {
	HomePage home;
	CadastroFuncionarioPage cadastro;
	EditarFuncionarioPage edita;
	DeletarFuncionarioPage remove;

	public CadastroFuncionarioStep() {
		home = new HomePage();
		cadastro = new CadastroFuncionarioPage();
		edita = new EditarFuncionarioPage();
		remove = new DeletarFuncionarioPage();
	}

	@Quando("eu acesso à pagina de gestão de funcionários")
	public void acessoPage() {
		home.checkFuncionariosPage();
	}

	@Quando("eu acesso ao cadastro de novo funcionários")
	public void novoCadastro() {
		cadastro.clicarBtNovoFuncionario();
	}

	@Então("preencho o nome {string}, cpf {string}, seleciono sexo {string}, digito a data de admissão {string}, cargo {string} e salário {string}")
	public void preenchimentoCadastro(String nome, String cpf, String sexo, String admissao, String cargo,
			String salario) {
		cadastro.preencherNome(nome);
		cadastro.preencherCPF(cpf);
		cadastro.escolherSexo(sexo);
		cadastro.preencherAdmissao(admissao);
		cadastro.preencherCargo(cargo);
		cadastro.preencherSalario(salario);
		cadastro.clicarContratacao();
		cadastro.clicarEnviar();
	}

	@Então("devo ver a mensagem e sucesso {string}")
	public void confirmarCadastro(String sucessMessage) {
		assertTrue(cadastro.getSuccessMessage(sucessMessage));
	}

	@Quando("procuro pelo funcionário pelo cpf {string}")
	public void procurarCadastro(String cpf) {
		edita.pesquisaFuncionario(cpf);
	}

	@Então("altero as informações de nome para {string}, Cargo para {string} e Salário para {string}")
	public void alterarCadastro(String Newnome, String Newcargo, String Newsalario) {
		edita.clicarBtEditarFuncionario();
		edita.alterarNome(Newnome);
		edita.alterarCargo(Newcargo);
		edita.alterarSalario(Newsalario);
		cadastro.clicarEnviar();

	}

	@Quando("procuro pelo funcionário pelo cpf {string} a ser deletado")
	public void searchCadastro(String cpf) {
		remove.pesquisaFuncionario(cpf);
		remove.clicarBtDeletarFuncionario();
	}
}
