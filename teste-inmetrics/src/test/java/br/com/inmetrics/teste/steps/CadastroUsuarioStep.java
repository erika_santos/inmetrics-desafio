package br.com.inmetrics.teste.steps;

import static org.junit.Assert.*;

import br.com.inmetrics.teste.pages.HomePage;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class CadastroUsuarioStep {
	HomePage home;

	public CadastroUsuarioStep() {
		home = new HomePage();
	}	

	
	/**
	 * **************************** Dado ****************************
	 */
	@Dado("que acesso a pagina da inmrobo")
	public void validateHomeScreen() {
		home.clicarBtCadastrese();
		assertTrue(home.checkLbCadastrese());
	}

	/**
	 * *************************** Quando ****************************
	 */
	
	@Quando("cadastro um usuário com nome de usuario {string}, a senha {string} e confirmo a mesma senha")
	public void realizarCadastro(String nome, String password) {
		home.preencherUsuario(nome);
		home.preencherSenha(password);
		home.confirmarSenha(password);
		home.clicarBtCdastrar();
	}
	
	/**
	 * **************************** Então *****************************
	 */
	
	@Então("retorno a pagina de login")
	public void confirmarCadastro() {
		assertTrue(home.checkLbLogin());
		
	}

}