package br.com.inmetrics.teste.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeletarFuncionarioPage extends BasePage {
	
	@FindBy(id = "delete-btn")
	protected WebElement btDeleteFuncionario;
	
	@FindBy(xpath = "//*[@id=\"tabela_filter\"]//input[@type=\"search\"]")
	protected WebElement stPesquisar;
		
	@FindBy(xpath = "//div[@class=\"alert alert-success alert-dismissible fade show\"]")
	protected WebElement lbSuccessMessage;	
	
	public void clicarBtDeletarFuncionario() {
		fixedWait(10);
		clickElement(btDeleteFuncionario);
		sleep(3000);
	}
	
	public void pesquisaFuncionario(String cpf) {
		clickElement(stPesquisar);
		setText(stPesquisar, cpf);
		sleep(5000);
	}
	
	public String getSuccessMessage() {
		waitOn(lbSuccessMessage);
		return getText(lbSuccessMessage);
	}
}
