package br.com.inmetrics.teste.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.inmetrics.teste.support.Hooks;

public class BasePage {

	
	protected WebDriver driver;
	protected WebDriverWait wait;

	public BasePage() {
		this.driver = Hooks.getDriver();
		wait = new WebDriverWait(driver, 20);
		PageFactory.initElements(driver, this);
	}

	public void waitOn(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void sleep(Integer milliseconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void fixedWait(int time) {
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	public void clickElement(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	public void setText(WebElement element, String value) {
		wait.until(ExpectedConditions.elementToBeClickable(element)).sendKeys(value);
	}

	public void keyboardAction(WebElement element, Keys keys) {
		element.sendKeys(keys);
	}

	public String getText(WebElement element) {
		return element.getText();
	}

	public void selectByVisibleText(WebElement combobox, String value) {
		wait.until(ExpectedConditions.elementToBeClickable(combobox));
		Select sel = new Select(combobox);
		sel.selectByVisibleText(value);
	}

	public boolean checkElementVisible(WebElement element) {
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void closeBrowser() {
		driver.close();
	}
}
