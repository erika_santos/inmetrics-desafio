package br.com.inmetrics.teste.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		  features = "src/test/resources/feature/", 
		  tags = "@regression",
		  glue = { "br/com/inmetrics/teste"},  
		  plugin = { "pretty","html:target/report-html.html"}, 
		  monochrome = true 
		)

public class RunCucumberTest {

}