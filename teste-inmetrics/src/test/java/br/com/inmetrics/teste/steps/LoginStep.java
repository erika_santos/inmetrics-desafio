package br.com.inmetrics.teste.steps;

import static org.junit.Assert.*;

import br.com.inmetrics.teste.pages.HomePage;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class LoginStep {
	HomePage home;

	public LoginStep() {
		home = new HomePage();
	}

	/**
	 * **************************** Dado ****************************
	 */
	
	@Dado("que eu entro na pagina da inmrobo")
	public void validateHomeScreen() {
		assertTrue(home.checkLbLogin());
	}

	/**
	 * *************************** Quando ****************************
	 */
	
	@Quando("eu faço o login com {string} e {string}")
	public void realizarLogin(String user, String password) {
		home.preencherUsuario(user);
		home.preencherSenha(password);
		home.clicarBtEntre();
	}

	/**
	 * **************************** Então *****************************
	 */
	@Então("a pagina de gestão de funcionários é acessada com sucesso")
	public void verificarLogin() {
		home.checkFuncionariosPage();

	}
}