package br.com.inmetrics.teste.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CadastroFuncionarioPage extends BasePage {

	@FindBy(xpath = "//div[@id=\"navbarSupportedContent\"]//a[@href=\"/empregados/new_empregado\"]")
	protected WebElement btNovoFuncionario;
	
	@FindBy(id = "inputNome")
	protected WebElement stNome;

	@FindBy(id = "cpf")
	protected WebElement stCPF;

	@FindBy(id = "slctSexo")
	protected WebElement cbSexo;
	
	@FindBy(id = "inputAdmissao")
	protected WebElement stAdmissao;

	@FindBy(id = "inputCargo")
	protected WebElement stCargo;

	@FindBy(name = "salario")
	protected WebElement stSalario;

	@FindBy(xpath = "//input[@id=\"clt\"]")
	protected WebElement rbClt;	
	
	@FindBy(xpath = "//input[@class=\"cadastrar-form-btn\"]")
	protected WebElement btEnviar;	
	
	@FindBy(xpath = "//div[@class=\"alert alert-success alert-dismissible fade show\"]")
	protected WebElement lbSuccessMessage;	

	public void clicarBtNovoFuncionario() {
		clickElement(btNovoFuncionario);
	}

	public void escolherSexo(String sexo) {
		selectByVisibleText(cbSexo, sexo);
	}

	public void preencherNome(String nome) {
		clickElement(stNome);
		setText(stNome, nome);
	}
	
	public void preencherCPF(String cpf) {
		clickElement(stCPF);
		setText(stCPF, cpf);
	}
	
	public void preencherAdmissao(String admissao) {
		clickElement(stAdmissao);
		setText(stAdmissao, admissao);
	}
	
	public void preencherCargo(String cargo) {
		clickElement(stCargo);
		setText(stCargo, cargo);
	}
	
	public void preencherSalario(String salario) {
		clickElement(stSalario);
		setText(stSalario, salario);
	}
	
	public void clicarContratacao() {
		clickElement(rbClt);
	}
	
	public void clicarEnviar() {
		clickElement(btEnviar);
	}

	
	public boolean getSuccessMessage(String message) {
		waitOn(lbSuccessMessage);
		
		if (getText(lbSuccessMessage).contains(message)) {
			return true;
		} else {
			return false;
		}
	}
	
}
