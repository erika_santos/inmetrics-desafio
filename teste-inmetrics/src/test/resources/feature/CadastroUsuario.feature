#language: pt

Funcionalidade: Cadastrar Usuário
    Para acesso ao site de gestão de funcionários
    Devo criar uma conta de usuário

    @cadastro_usuario @regression
    Cenario: CadastroUsuario
        Dado que acesso a pagina da inmrobo        
        Quando cadastro um usuário com nome de usuario "TestAutomation03", a senha "123456" e confirmo a mesma senha
        Então retorno a pagina de login
