#language: pt

Funcionalidade: Cadastrar Funcionário
  Para  cadastrar um novo funcionário no site de gestão de funcionários
  Devo estar devidamente logado com um usuário


  Contexto: Autenticar
    Dado que eu entro na pagina da inmrobo
    Quando eu faço o login com "TestAutomation01" e "123456"
    Quando eu acesso à pagina de gestão de funcionários


  @cadastro_funcionario @regression
  Cenario: Cadastrar Funcionario
    E eu acesso ao cadastro de novo funcionários
    Então preencho o nome "teste_automation", cpf "897.792.524-01", seleciono sexo "Feminino", digito a data de admissão "02032020", cargo "Analista" e salário "2500"
    E devo ver a mensagem e sucesso "Usuário cadastrado com sucesso"


  @edita_funcionario @regression
  Cenario: Editar o cadastro do Funcionario
    E procuro pelo funcionário pelo cpf "897.792.524-01"
    Então altero as informações de nome para "Teste Edicao", Cargo para "Analista Pleno" e Salário para "800000"
    E devo ver a mensagem e sucesso "Informações atualizadas com sucesso"


  @remove_funcionario @regression
  Cenario: Remove o cadastro do Funcionario
    E procuro pelo funcionário pelo cpf "897.792.524-01" a ser deletado
    Então devo ver a mensagem e sucesso "Funcionário removido com sucesso"
        
        
        