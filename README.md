## Teste Inmetrics - Descrever como executar o projeto

# Pré - requisitos
git clone https://gitlab.com/erika_santos/inmetrics-desafio.git

# Ambiente 
* Web Site: http://http://www.inmrobo.tk/accounts/login/

# Cenários 

Nome Cenário 		    | 	Tag
------------------- | -------------------------
Cadastrar Usuário	| @cadastro_usuario
Login			    | @login
Cadastar Funcionário| @cadastro_funcionario
Editar Funcionário	| @edita_funcionario
Remover Funcionário	| @remove_funcionario

# Execução dos Cenário:
`
cucumber


**Exemplo:** 
`
  @CucumberOptions(
		  tags = "@edita_funcionario" 
